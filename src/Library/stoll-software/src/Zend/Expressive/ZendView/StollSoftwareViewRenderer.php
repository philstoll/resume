<?php

declare(strict_types=1);

namespace StollSoftware\Zend\Expressive\ZendView;

use Zend\Expressive\Template\TemplateRendererInterface;

class StollSoftwareViewRenderer implements TemplateRendererInterface
{
    private $template;

    public function __construct(TemplateRendererInterface $template, $googleAnaliticsId = null)
    {
        $this->template = $template;

        if (null !== $googleAnaliticsId) {
            $this->addDefaultParam(TemplateRendererInterface::TEMPLATE_ALL, 'google_analytics_id', $googleAnaliticsId);
        }
    }

    public function render(string $name, $params = []) : string
    {
        return $this->template->render($name, $params);
    }

    public function addPath(string $path, string $namespace = null) : void
    {
        $this->template->addPath($path, $namespace);
    }

    public function getPaths() : array
    {
        return $this->template->getPaths();
    }

    public function addDefaultParam(string $templateName, string $param, $value) : void
    {
        $this->template->addDefaultParam($templateName, $param, $value);
    }
}
